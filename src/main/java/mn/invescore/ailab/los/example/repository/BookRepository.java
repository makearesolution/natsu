package mn.invescore.ailab.los.example.repository;

import mn.invescore.ailab.los.example.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface BookRepository extends BaseRepository<Book, Long> {

    Page<Book> findByTitleContainsOrContentContains(String title, String content, Pageable pageable);

    List<Book> findByTitleStartingWith(String title);
/*
    @Query("select id from Book id where id = ?1 ")
    Book findOne(Long id);
*/

}
