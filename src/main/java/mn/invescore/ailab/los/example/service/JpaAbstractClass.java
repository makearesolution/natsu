package mn.invescore.ailab.los.example.service;

import com.sun.xml.internal.bind.v2.model.core.ID;
import mn.invescore.ailab.los.example.model.BaseModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class JpaAbstractClass<T extends BaseModel, ID> implements JpaInterface<T, ID> {

    protected T save(T model) {
        try {
            return getRepository().save(model);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    // @PostAuthorize("hasPermission('returnObject', 'READ_MODEL')")
    public Page<T> findAll(Pageable pageable) {
        return getRepository().findAll(pageable);
    }

    @Override
    // @PostAuthorize("hasPermission('returnObject', 'READ_MODEL')")
    public T findById(ID id) {
        Optional<T> result = getRepository().findById(id);
        return result.orElse(null);
    }



    @Override
    // @PreAuthorize("hasPermission('#id', 'DELETE_MODEL')")
    public boolean existsById(@Param("id") ID id) {
        return getRepository().existsById(id);
    }

    @Override
    // @PreAuthorize("hasPermission(#model, 'DELETE_MODEL')")
    public boolean delete(T model) {
        try {
            getRepository().delete(model);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    // @PreAuthorize("hasPermission(#id, 'DELETE_MODEL')")
    public boolean deleteById(@Param("id") ID id) {
        try {

            getRepository().deleteById(id);

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    // @PostAuthorize("hasPermission('returnObject', 'READ_MODEL')")
    public List<T> findAll() {
        Iterable<T> temp = getRepository().findAll();

        List<T> result = new ArrayList<>();
        temp.forEach(result::add);

        return result;
    }





    public void deleteAll() {
        getRepository().deleteAll();
    }
}
