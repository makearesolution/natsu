package mn.invescore.ailab.los.example.repository;

import mn.invescore.ailab.los.example.model.Author;
import mn.invescore.ailab.los.example.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AuthorRepository extends BaseRepository<Author, Long> {

    Page<Author> findByLastNameContainsOrFirstNameContains(String lastName, String firstName, Pageable pageable);

    List<Author> findByLastNameStartingWith(String lastName);

  //  Page<Author> findByLastName(Long id);
}