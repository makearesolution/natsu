package mn.invescore.ailab.los.example.model;


import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "example_book")
public class Book extends BaseModel {

    @Column(name = "title", nullable = false, length = 150)
    @Size(min = 6, max = 150)

    private String title;

    @Column(name = "content")
    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    @ManyToOne
    @JoinColumn(name="authorId")
    private Author author;

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
