package mn.invescore.ailab.los.example.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"mn.invescore.ailab"})
public class SpringRootConfig {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}