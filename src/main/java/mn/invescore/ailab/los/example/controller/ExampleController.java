package mn.invescore.ailab.los.example.controller;

import mn.invescore.ailab.los.example.Utils;
import mn.invescore.ailab.los.example.model.Book;
import mn.invescore.ailab.los.example.service.AuthorService;
import mn.invescore.ailab.los.example.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/example")//Request prefix
public class ExampleController extends AbstractController<Book, Long> {

    @Autowired
    BookService bookService;

    @Autowired
    AuthorService authorService;

    private static final Logger logger = LoggerFactory.getLogger(ExampleController.class);

    /*
     * Моделийн хуудаслалттай жагсаалт
     */
//    @PreAuthorize("hasPermission('returnObject', 'READ_PRIVILEGE')")
    @GetMapping
    public ModelAndView index(@RequestParam(value = "pageSize", required = false) Optional<Integer> pageSize,
                              @RequestParam(value = "page", required = false) Optional<Integer> page,
                              @RequestParam(value = "sort", required = false) List<String> sort,
                              @RequestParam(value = "q", required = false) String q) {

        int evalPageSize = pageSize.orElse(this.getPageSize());
        int evalPage = (page.orElse(0) < 1) ? 0 : page.get() - 1;


        ModelAndView view = new ModelAndView();
        view.setViewName("/example/index.html");

        Pageable pageable = Utils.getPage(evalPage, evalPageSize, sort);

        Page<Book> data;
        if (q != null && q.trim().length() > 0) {
            view.addObject("q", q.trim());
            data = bookService.findByContains(q, pageable);
        } else {
            data = bookService.findAll(pageable);
        }

        view.addObject("data", data);
//
//        ViewPermission permission = new ViewPermission(true, true, true, true, true);
//        view.addObject("perm", permission);

        return view;
    }

    /*
     * id - тай моделийн дэлгэрэнгүй мэдээлэл харах
     */
    @GetMapping("/{id}")
    public ModelAndView view(@PathVariable("id") Long id, RedirectAttributes redirectAttrs) {

        Book book = bookService.findById(id);
        if (book == null) {
            redirectAttrs.addFlashAttribute(Utils.MESSAGE_WARNING, "Номын мэдээлэл олдсонгүй");
            return new ModelAndView("redirect:/example");
        }

        ModelAndView view = new ModelAndView("example/view.html");
        view.addObject("book", book);
//        view.addObject("authors", authorService.findById(book.getAuthorId()));
        return view;
    }


    /*
     * шинэ модель үүсгэх
     */
    @GetMapping("/create")
    public ModelAndView create() {

        ModelAndView view = new ModelAndView();
        view.setViewName("example/create.html");


        view.addObject("book", new Book() );
        view.addObject("authors", authorService.findAll());
        return view;
    }

    /*
     * шинэ модель хадгалах
     */
    @PostMapping("/store")
    public ModelAndView store(@ModelAttribute("book") @Valid Book book, BindingResult bindingResult, RedirectAttributes redirectAttrs) {
        if (bindingResult.hasErrors()) {
            return new ModelAndView("/example/create.html", bindingResult.getModel());
        }


        ModelAndView view = new ModelAndView();
        view.setViewName("/example/create.html");

        book.setId(Utils.uniqueId());
        try {
            bookService.create(book);
        } catch (Exception e) {
            e.printStackTrace();
            view.addObject(Utils.MESSAGE_WARNING, "Номын мэдээлэл шалгаад дахин оролдно уу!");
            view.addObject("book", book);
            view.addObject("authors", authorService.findAll());
            return view;
        }

        redirectAttrs.addFlashAttribute(Utils.MESSAGE_SUCCESS, "Номын мэдээлэл амжилттай нэмлээ");

        view.setViewName("redirect:/example/" + book.getId());
        return view;
    }

    /*
     * засах модель хадгалах
     */
    @GetMapping("/{id}/edit")
    public ModelAndView edit(@PathVariable("id") Long id, RedirectAttributes redirectAttrs) {
        ModelAndView view = new ModelAndView();
        view.setViewName("example/edit.html");

        Book book = bookService.findById(id);
        if (book == null) {
            redirectAttrs.addFlashAttribute(Utils.MESSAGE_WARNING, "Номын мэдээлэл олдсонгүй");
            return new ModelAndView("redirect:/example");
        }

        view.addObject("book", book);
        view.addObject("authors", authorService.findAll());

        return view;

    }


    /*
     * засах модель хадгалах
     */

    @PostMapping("/{id}")
    public ModelAndView update(@ModelAttribute("book") @Valid Book book, @PathVariable("id") Long id, BindingResult bindingResult, RedirectAttributes redirectAttrs) {

        if (bindingResult.hasErrors()) {
            if (bindingResult.hasErrors()) {
                return new ModelAndView("/example/edit.html", bindingResult.getModel());
            }
        }

        ModelAndView view = new ModelAndView();
        view.setViewName("/example/edit.html");

        Book oldBook = bookService.findById(id);
        if (oldBook == null) {

            redirectAttrs.addFlashAttribute(Utils.MESSAGE_WARNING, "Номын мэдээлэл засах боломжгүй.");
            view.setViewName("redirect:/example/" + book.getId());
            return view;
        }

        book.setId(id);
        try {
            bookService.update(book, oldBook);
        } catch (Exception e) {
            e.printStackTrace();
            view.addObject(Utils.MESSAGE_WARNING, "Номын мэдээлэл шалгаад дахин оролдно уу!");
            view.addObject("book", book);
//            view.addObject("authors", authorService.findById(book.getAuthorId()));
            return view;
        }

        redirectAttrs.addFlashAttribute(Utils.MESSAGE_SUCCESS, "Номын мэдээлэл амжилттай заслаа.");

        view.setViewName("redirect:/example/" + book.getId());
        return view;
    }

    /*
     * id - тай моделийн мэдээлэл устгах
     */
    @GetMapping("/{id}/delete")
    public ModelAndView destroy(@PathVariable("id") Long id, RedirectAttributes redirectAttrs) {

        Book book = bookService.findById(id);

        if (book == null) {
            redirectAttrs.addFlashAttribute(Utils.MESSAGE_WARNING, "Номын мэдээлэл байхгүй байна.");
            return new ModelAndView("redirect:/example");
        }

        if (bookService.delete(book)) {
            redirectAttrs.addFlashAttribute(Utils.MESSAGE_SUCCESS, "Номын мэдээлэл амжилттай устгалаа.");
            return new ModelAndView("redirect:/example");
        } else {
            redirectAttrs.addFlashAttribute(Utils.MESSAGE_WARNING, "Номын мэдээлэл устгаж чадсангүй.");
            return new ModelAndView("redirect:/example");
        }

    }
}
