package mn.invescore.ailab.los.example.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan({"mn.invescore.ailab"})
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${security.enable.csrf}")
    private boolean csrfEnabled;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        if (!csrfEnabled)
            http.cors().and().csrf().disable();
        http.authorizeRequests()
                .anyRequest().anonymous();
    }
}