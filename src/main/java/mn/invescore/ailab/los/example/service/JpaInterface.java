package mn.invescore.ailab.los.example.service;

import mn.invescore.ailab.los.example.model.BaseModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface JpaInterface<T extends BaseModel, ID> {

    PagingAndSortingRepository<T, ID> getRepository();

    T create(T model) throws Exception;

    T update(T model, T oldModel);

    Page<T> findAll(Pageable pageable);

    Iterable<T> findAll();

    T findById(ID id);

    boolean existsById(ID id);

    boolean delete(T model);

    boolean deleteById(ID id);



}

