package mn.invescore.ailab.los.example;


import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {

    public final static String MESSAGE_SUCCESS = "message_success";

    public final static String MESSAGE_INFO = "message_info";

    public final static String MESSAGE_WARNING = "message_warning";

    public final static String MESSAGE_DANGER = "message_danger";



    public static Pageable getPage(int page, int size, List<String> sorts) {

        ArrayList<Sort.Order> sortFields = new ArrayList<>();
        if (sorts != null) {
            for (String sortItem : sorts) {
                if (sortItem.endsWith(".desc")) {
                    sortFields.add(Sort.Order.desc(sortItem.substring(0, sortItem.length() - 5)));
                } else {
                    if (!sortItem.endsWith("asc")) {
                        sortItem = sortItem + ".asc"; // default sort order of current column is asc
                    }
                    sortFields.add(Sort.Order.asc(sortItem.substring(0, sortItem.length() - 4)));
                }
            }
        }

        if (sortFields.size() > 0) {
            return PageRequest.of(page, size, Sort.by(sortFields));
        } else {
            return PageRequest.of(page, size);
        }
    }

    public static synchronized Long uniqueId() {
        Long uniqId = 0L;
        Random rand = new Random();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        uniqId += (calendar.get(Calendar.YEAR) % 100 * 12 + calendar.get(Calendar.MONTH)) * 31 + calendar.get(Calendar.DATE) - 1;
        uniqId = (((uniqId * 24 + calendar.get(Calendar.HOUR)) * 60 + calendar.get(Calendar.MINUTE)) * 60 + calendar.get(Calendar.SECOND)) * 1000 + calendar.get(Calendar.MILLISECOND);
        uniqId = uniqId * 100 + rand.nextInt(100);

        return uniqId;
    }
}
