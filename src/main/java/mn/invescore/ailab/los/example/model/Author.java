package mn.invescore.ailab.los.example.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

@Entity
@Table(name = "example_author")
public class Author extends BaseModel {
    @Column(name = "lastname", nullable = false, length = 40)
    @Size(min = 6, max = 40)

    private String lastName;

    @Column(name="firstname", nullable=false, length = 40)
    private  String firstName;

    public String getLastName(){return this.lastName;}

    public void setLastName(String lastname){this.lastName=lastname;}

    public String getFirstName(){return this.firstName;}

    public void  setFirstName(String firstname){this.firstName=firstname;}

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true )
    @JoinColumn(name="authorId")
    public List<Book> books;

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
