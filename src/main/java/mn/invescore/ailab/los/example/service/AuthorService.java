package mn.invescore.ailab.los.example.service;

import com.sun.xml.internal.bind.v2.model.core.ID;
import mn.invescore.ailab.los.example.model.Author;
import mn.invescore.ailab.los.example.model.Book;
import mn.invescore.ailab.los.example.repository.AuthorRepository;
import mn.invescore.ailab.los.example.repository.BaseRepository;
import mn.invescore.ailab.los.example.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthorService extends JpaAbstractClass<Author, Long> {

    @Autowired
    private AuthorRepository authorRepository;

    @Override
    public BaseRepository<Author, Long> getRepository() {
        return this.authorRepository;
    }

    /*
     * q параметрээр орж ирсэн утгаар example_book хүснэгтээс title болон content талбаруудаар хайлт хийх
     */

    public Page<Author> findByLastName(String q, Pageable pageable) {
        return authorRepository.findByLastNameContainsOrFirstNameContains(q, q, pageable);
    }

   /* @Override
    public Page<Author> findBylastName(Long id){
        return authorRepository.findByLastName(id);
    }*/
    @Override
    public Author create(Author model) {
        return super.save(model);
    }

    @Override
    public Author update(Author model, @Param("oldModel") Author oldModel) {
        return super.save(model);
    }



}
