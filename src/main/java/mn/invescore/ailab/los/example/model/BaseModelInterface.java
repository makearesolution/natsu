package mn.invescore.ailab.los.example.model;

public interface BaseModelInterface {
    Long getId();
    void setId(Long id);
}
