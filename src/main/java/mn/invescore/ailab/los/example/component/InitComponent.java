package mn.invescore.ailab.los.example.component;

import org.springframework.stereotype.Component;

@Component("initExample")
public class InitComponent {

    public void init() {
    }

}
