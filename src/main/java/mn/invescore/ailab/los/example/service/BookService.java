package mn.invescore.ailab.los.example.service;

import mn.invescore.ailab.los.example.model.Book;
import mn.invescore.ailab.los.example.repository.BaseRepository;
import mn.invescore.ailab.los.example.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

@Service
public class BookService extends JpaAbstractClass<Book, Long> {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public BaseRepository<Book, Long> getRepository() {
        return this.bookRepository;
    }

    /*
     * q параметрээр орж ирсэн утгаар example_book хүснэгтээс title болон content талбаруудаар хайлт хийх
     */

    public Page<Book> findByContains(String q, Pageable pageable) {
        return bookRepository.findByTitleContainsOrContentContains(q, q, pageable);
    }

  /*  public Page<Book> findOne(Long id){
        return bookRepository.findOne(id);
    }*/

    @Override
    public Book create(Book model) {
        return super.save(model);
    }

    @Override
    public Book update(Book model, @Param("oldModel") Book oldModel) {
        return super.save(model);
    }

}
