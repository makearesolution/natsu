package mn.invescore.ailab.los.example.controller;

import mn.invescore.ailab.los.example.model.BaseModel;
import org.springframework.beans.factory.annotation.Value;

public abstract class AbstractController<T extends BaseModel, ID> {
    @Value("${page.size}")
    protected int pageSize = 15;

    public static String KEY_BREADCRUMBS = "breadcrumbs";
    public static String KEY_HEADER = "header";

    public int getPageSize() {
        return pageSize;
    }
}
