package mn.invescore.ailab.los.example.exception;

public class DoesNotExistException extends AppException {
    public DoesNotExistException() {
        super();
    }

    public DoesNotExistException(String message) {
        super(message);
    }
}
