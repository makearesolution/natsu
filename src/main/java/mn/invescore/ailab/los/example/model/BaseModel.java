package mn.invescore.ailab.los.example.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import mn.invescore.ailab.los.example.Utils;
import mn.invescore.ailab.los.example.exception.DoesNotExistException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseModel implements BaseModelInterface, Serializable, Cloneable {

    private static final Logger logger = LoggerFactory.getLogger(BaseModel.class);

    @Transient
    protected BaseModel baseClone;

    @Id
    @Column(name = "id", length = 50, updatable = false, nullable = false)
    protected Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm")
    @Column(name = "created_at", updatable = false)
    @CreatedDate
    protected Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm")
    @Column(name = "updated_at")
    @LastModifiedDate
    protected Date updatedAt;

    @Column(name = "created_by", updatable = false)
    protected Long createdBy;

    @Column(name = "updated_by")
    protected Long updatedBy;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    @PostLoad
    protected void saveState() {
    }

    @PrePersist
    public void onPrePersist() {
        if (this.id == null || this.id <= 0)
            this.setId(Utils.uniqueId());
    }

    @PreUpdate
    public void onPreUpdate() {
    }

    @PreRemove
    public void onPreRemove() {
        System.out.println("-----------DELETE BEFORE---------------");
    }

    @Override
    protected BaseModel clone() {

        BaseModel clone = null;
        try {
            clone = (BaseModel) super.clone();
        } catch (CloneNotSupportedException cns) {
            logger.error("Error while cloning programmer", cns);
        }

        logger.debug("clone success.");

        return clone;

    }

    @Override
    public boolean equals(Object obj) {
        if (super.equals(obj)) return true;

        if (obj instanceof BaseModel) {
            BaseModel temp = (BaseModel) obj;

            return this.getId().equals(temp.getId());
        }
        return false;
    }

    public static <T extends BaseModel> T requireNonNull(T object, String message) {
        if (object == null) {
            throw new DoesNotExistException(message);
        }

        return object;
    }
}
