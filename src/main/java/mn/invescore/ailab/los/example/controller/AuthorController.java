package mn.invescore.ailab.los.example.controller;

import mn.invescore.ailab.los.example.Utils;
import mn.invescore.ailab.los.example.model.Author;
import mn.invescore.ailab.los.example.model.Author;
import mn.invescore.ailab.los.example.model.Book;
import mn.invescore.ailab.los.example.service.AuthorService;
import mn.invescore.ailab.los.example.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/author")//Request prefix
public class AuthorController extends AbstractController<Author,Long> {

    @Autowired
    AuthorService authorService;

    @Autowired
    BookService bookService;

    private static final Logger logger = LoggerFactory.getLogger(AuthorController.class);

    @GetMapping
    public ModelAndView index(@RequestParam(value = "pageSize", required = false) Optional<Integer> pageSize,
                              @RequestParam(value = "page", required = false) Optional<Integer> page,
                              @RequestParam(value = "sort", required = false) List<String> sort,
                              @RequestParam(value = "q", required = false) String q) {

        int evalPageSize = pageSize.orElse(this.getPageSize());
        int evalPage = (page.orElse(0) < 1) ? 0 : page.get() - 1;


        ModelAndView view = new ModelAndView();
        view.setViewName("/Author/index.html");

        Pageable pageable = Utils.getPage(evalPage, evalPageSize, sort);

        Page<Author> data;
        if (q != null && q.trim().length() > 0) {
            view.addObject("q", q.trim());
            data = authorService.findByLastName(q, pageable);
        } else {
            data = authorService.findAll(pageable);
        }

        view.addObject("data", data);
//
//        ViewPermission permission = new ViewPermission(true, true, true, true, true);
//        view.addObject("perm", permission);

        return view;
    }

    /*
     * id - тай моделийн дэлгэрэнгүй мэдээлэл харах
     */
    @GetMapping("/{id}")
    public ModelAndView view(@PathVariable("id") Long id, RedirectAttributes redirectAttrs) {

        Author author = authorService.findById(id);
        if (author == null) {
            redirectAttrs.addFlashAttribute(Utils.MESSAGE_WARNING, "Зохиогчийн мэдээлэл олдсонгүй");
            return new ModelAndView("redirect:/author");
        }

        ModelAndView view = new ModelAndView("Author/view.html");
        view.addObject("author", author);
//        view.addObject("books", bookService.findById(author.getBookId()));

        return view;
    }

    /*
     * шинэ модель үүсгэх
     */
    @GetMapping("/create")
    public ModelAndView create() {

        ModelAndView view = new ModelAndView();
        view.setViewName("Author/create.html");

        view.addObject("author", new Author());


        return view;
    }

    /*
     * шинэ модель хадгалах
     */
    @PostMapping("/store")
    public ModelAndView store(@ModelAttribute("book") @Valid Author author, BindingResult bindingResult, RedirectAttributes redirectAttrs) {
        if (bindingResult.hasErrors()) {
            return new ModelAndView("/Author/create.html", bindingResult.getModel());
        }


        ModelAndView view = new ModelAndView();
        view.setViewName("/Author/create.html");

        author.setId(Utils.uniqueId());
        try {
            authorService.create(author);
        } catch (Exception e) {
            e.printStackTrace();
            view.addObject(Utils.MESSAGE_WARNING, "Зохиогчийн мэдээлэл шалгаад дахин оролдно уу!");
            view.addObject("author", author);
//            author.books.add(author.getBookId());
            view.addObject("books" , author.books);
            return view;
        }

        redirectAttrs.addFlashAttribute(Utils.MESSAGE_SUCCESS, "Зохиогчийн мэдээлэл амжилттай нэмлээ");

        view.setViewName("redirect:/author/" + author.getId());
        return view;
    }

    /*
     * засах модель хадгалах
     */
    @GetMapping("/{id}/edit")
    public ModelAndView edit(@PathVariable("id") Long id, RedirectAttributes redirectAttrs) {
        ModelAndView view = new ModelAndView();
        view.setViewName("Author/edit.html");

        Author author = authorService.findById(id);
        if (author == null) {
            redirectAttrs.addFlashAttribute(Utils.MESSAGE_WARNING, "Зохиогчийн мэдээлэл олдсонгүй");
            return new ModelAndView("redirect:/author");
        }

        view.addObject("author", author);
      view.addObject("books", bookService.findAll());

        //author.books.add(bookService.findById(author.getBookId()));
        //view.addObject("books", author.books);
      //  view.addObject("book", bookService.findAll());

        return view;

    }

    /*
     * засах модель хадгалах
     */

    @PostMapping("/{id}")
    public ModelAndView update(@ModelAttribute("author") @Valid Author author, @PathVariable("id") Long id, BindingResult bindingResult, RedirectAttributes redirectAttrs) {

        if (bindingResult.hasErrors()) {
            if (bindingResult.hasErrors()) {
                return new ModelAndView("/Author/edit.html", bindingResult.getModel());
            }
        }

        ModelAndView view = new ModelAndView();
        view.setViewName("/Author/edit.html");

        Author oldAuthor = authorService.findById(id);
        if (oldAuthor == null) {

            redirectAttrs.addFlashAttribute(Utils.MESSAGE_WARNING, "Зохиогчийн мэдээлэл засах боломжгүй.");
            view.setViewName("redirect:/author/" + author.getId());
            return view;
        }

        author.setId(id);
        try {
            authorService.update(author, oldAuthor);
        } catch (Exception e) {
            e.printStackTrace();
            view.addObject(Utils.MESSAGE_WARNING, "Номын мэдээлэл шалгаад дахин оролдно уу!");
            view.addObject("author", author);
//            view.addObject("books", bookService.findById(author.getBookId()));
            return view;
        }

        redirectAttrs.addFlashAttribute(Utils.MESSAGE_SUCCESS, "Зохиогчийн мэдээлэл амжилттай заслаа.");

        view.setViewName("redirect:/author/" + author.getId());
        return view;
    }


    /*
     * id - тай моделийн мэдээлэл устгах
     */
    @GetMapping("/{id}/delete")
    public ModelAndView destroy(@PathVariable("id") Long id, RedirectAttributes redirectAttrs) {

        Author author = authorService.findById(id);

        if (author == null) {
            redirectAttrs.addFlashAttribute(Utils.MESSAGE_WARNING, "{Зохиогчийн  мэдээлэл байхгүй байна.");
            return new ModelAndView("redirect:/author");
        }

        if (authorService.delete(author)) {
            redirectAttrs.addFlashAttribute(Utils.MESSAGE_SUCCESS, "Зохиогчийн  мэдээлэл амжилттай устгалаа.");
            return new ModelAndView("redirect:/author");
        } else {
            redirectAttrs.addFlashAttribute(Utils.MESSAGE_WARNING, "Зохиогчийн  мэдээлэл устгаж чадсангүй.");
            return new ModelAndView("redirect:/author");
        }

    }

}
