package mn.invescore.ailab.los.example.repository;

import mn.invescore.ailab.los.example.model.BaseModel;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

@NoRepositoryBean
public interface BaseRepository<T extends BaseModel, ID> extends PagingAndSortingRepository<T, ID> {
}
